/**
 * @file TestPerturbationForce.h
 *
 * \brief Tests the PerturbationForce plugin on the tug of war model.
 *
 * @author Dimitar Stanev <dimitarstanev@epfl.ch>
 */
#include <iostream>
#include <OpenSim/OpenSim.h>
#include "PerturbationForce.h"

using namespace std;
using namespace OpenSim;

int main(int argc, char *argv[]) {
    try {
	Model model("tug_of_war.osim");
	// model.setUseVisualizer(true);
	// force
	auto perturbationForce = new PerturbationForce();
	perturbationForce->setName("noise");
	perturbationForce->set_body_name("block");
	perturbationForce->set_offset(SimTK::Vec3(0, 0, 0));
	perturbationForce->set_magnitude(1000);
	model.addForce(perturbationForce);
	// simulation
	auto state = model.initSystem();
	simulate(model, state, 1, true);
	// output
	model.print("output_model.osim");
    }
    catch (exception &e) {
        cout << e.what() << endl;
        getchar();
        return -1;
    }
    return 0;
}

